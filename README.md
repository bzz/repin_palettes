#    'en',
Repin palettes
# Short description
App to create small pixel art sprites
## Full description
This is a small but powerful pixel art creation tool. Compose your own palette. Make sprites with transparency, pixel wise. Create bigger pictures, spot by spot. Share them with friends! There are more features coming! Explore... Repin palettes!
The app supports older devices (Android version 4.1.0 and higher).

#    'es',
Paletas Repin
# Breve descripción
Aplicación para crear pequeños sprites de pixel art
## Descripción completa
Esta es una herramienta de creación de pixel art pequeña pero poderosa. Crea tu propia paleta. Crea sprites con transparencia, píxeles. Cree imágenes más grandes, punto por punto. ¡Compártelos con amigos! ¡Hay más funciones por venir! Explora ... ¡Paletas Repin!
La aplicación es compatible con dispositivos más antiguos (versión de Android 4.1.0 y superior).

#    'pt',
Paletas Repin
# Pequena descrição
App para criar pequenos sprites pixel art
## Descrição completa
Esta é uma ferramenta de criação de pixel art pequena, mas poderosa. Componha sua própria paleta. Faça sprites com transparência, pixel sábio. Crie fotos maiores, ponto por ponto. Compartilhe-os com amigos! Há mais recursos chegando! Explorar ... Paletas Repin!
O aplicativo é compatível com dispositivos mais antigos (Android versão 4.1.0 e superior).

#    'fr',
Palettes Repin
# Brève description
Application pour créer de petits sprites pixel art
## Description complète
Il s'agit d'un petit mais puissant outil de création de pixel art. Composez votre propre palette. Créez des sprites avec transparence, au niveau des pixels. Créez des images plus grandes, point par point. Partagez-les avec vos amis! Il y a plus de fonctionnalités à venir! Explorez ... les palettes Repin!
L'application prend en charge les appareils plus anciens (version Android 4.1.0 et supérieure).

#    'de',
Repin-Paletten
# Kurze Beschreibung
App zum Erstellen kleiner Pixelkunst-Sprites
## Gesamte Beschreibung
Dies ist ein kleines, aber leistungsstarkes Tool zur Erstellung von Pixelkunst. Stellen Sie Ihre eigene Palette zusammen. Erstellen Sie Sprites mit Transparenz, pixelweise. Erstellen Sie größere Bilder, Punkt für Punkt. Teile sie mit Freunden! Es kommen weitere Funktionen! Entdecken Sie ... Repin-Paletten!
Die App unterstützt ältere Geräte (Android Version 4.1.0 und höher).

#    'tr',
Repin paletleri
# Kısa Açıklama
Küçük piksel sanat eseri oluşturma uygulaması
## Tam tanım
Bu, küçük ama güçlü bir piksel sanatı oluşturma aracıdır. Kendi paletinizi oluşturun. Piksel bilge, şeffaflıkla sprite oluşturun. Tek tek daha büyük resimler oluşturun. Arkadaşlarınızla paylaşın! Daha fazla özellik geliyor! Keşfedin ... Repin paletleri!
Uygulama daha eski cihazları (Android sürüm 4.1.0 ve üstü) destekler.

#    'ru',
Repin palettes
# Краткое описание
Приложение для создания спрайтов пиксель-арта
## Полное описание
Это небольшой, но мощный инструмент для создания пиксельной графики. Составьте свою собственную палитру. Создавайте спрайты с прозрачностью по пикселям. Создавайте более крупные изображения, точка за точкой. Поделись ими с друзьями! Впереди еще много функций! Исследуй ... Repin palettes!
Приложение поддерживает старые устройства (версия Android 4.1.0 и выше).

#    'id',
Repin palettes
# Deskripsi Singkat
Aplikasi untuk membuat sprite seni piksel kecil
## Deskripsi lengkap
Ini adalah alat kreasi seni piksel yang kecil tapi kuat. Buat palet Anda sendiri. Buat sprite dengan transparansi, piksel bijaksana. Buat gambar yang lebih besar, titik demi titik. Bagikan dengan teman! Ada lebih banyak fitur yang akan datang! Jelajahi ... Repin palettes!
Aplikasi ini mendukung perangkat lama (Android versi 4.1.0 dan lebih tinggi).

#    'zh',
列宾的调色板
＃ 简短的介绍
应用程序创建小像素艺术精灵
＃＃ 详细描述
这是一个小巧但功能强大的像素艺术创作工具。 组成自己的调色板。 使精灵具有透明性，像素化。 逐点创建更大的图片。 与朋友分享！ 还有更多功能！ 探索...列宾的调色板！
该应用程序支持较旧的设备（Android 4.1.0及更高版本）。

#    'ko',
팔레트 리핀
# 간단한 설명
작은 픽셀 아트 스프라이트를 만드는 앱
## 전체 설명
이것은 작지만 강력한 픽셀 아트 제작 도구입니다. 나만의 팔레트를 작성하세요. 투명도 픽셀 단위로 스프라이트를 만듭니다. 더 큰 그림을 각 지점별로 만듭니다. 친구들과 공유하세요! 더 많은 기능이 제공됩니다! 탐색 ... 팔레트 리핀!
이 앱은 이전 기기 (Android 버전 4.1.0 이상)를 지원합니다.

#    'hi',
Repin palettes
# संक्षिप्त वर्णन
अनुप्रयोग छोटे पिक्सेल कला स्प्राइट बनाने के लिए
## पूर्ण विवरण
यह एक छोटा लेकिन शक्तिशाली पिक्सेल आर्ट निर्माण उपकरण है। अपने पैलेट की रचना करें। पारदर्शिता के साथ स्प्राइट बनाओ, पिक्सेल वार। बड़े चित्र बनाएं, स्पॉट द्वारा हाजिर करें। उन्हें दोस्तों के साथ साझा करें! अधिक सुविधाएँ आ रही हैं! अन्वेषण करें ... Repin palettes!
एप्लिकेशन पुराने उपकरणों (Android संस्करण 4.1.0 और उच्चतर) का समर्थन करता है।

#    'bn',
Repin palettes
# ছোট বিবরণ
ছোট পিক্সেল আর্ট স্প্রিট তৈরি করতে অ্যাপ্লিকেশন
## পূর্ণ বিবরণ
এটি একটি ছোট তবে শক্তিশালী পিক্সেল আর্ট তৈরির সরঞ্জাম। নিজের প্যালেটটি রচনা করুন। স্বচ্ছতার সাথে স্প্রিট তৈরি করুন, পিক্সেল বুদ্ধিমান। বড় বড় ছবি, স্পট স্পট তৈরি করুন। তাদের বন্ধুদের সাথে ভাগ করুন! আরও বৈশিষ্ট্য আসছে! এক্সপ্লোর করুন ... Repin palettes!
অ্যাপ্লিকেশনটি পুরানো ডিভাইসগুলিকে (অ্যান্ড্রয়েড সংস্করণ 4.1.0 এবং উচ্চতর) সমর্থন করে।

#    'ar',
لوحات ريبين
# وصف قصير
التطبيق لإنشاء نقوش فنية صغيرة بكسل
## وصف كامل
هذه أداة إنشاء فن بكسل صغيرة ولكنها قوية. قم بتكوين لوح الألوان الخاص بك. اصنع نقوشًا متحركة بشفافية ووضوح بكسل. إنشاء صور أكبر ، بقعة تلو الأخرى. شاركها مع الأصدقاء! هناك المزيد من الميزات القادمة! استكشاف ...  لوحات ريبين!
يدعم التطبيق الأجهزة القديمة (إصدار Android 4.1.0 والإصدارات الأحدث).

#    'ur',
پیلیٹ ريبين
# مختصر کوائف
چھوٹے پکسل آرٹ اسپرٹس بنانے کے لئے ایپ
## مکمل تفصیل
یہ ایک چھوٹا لیکن طاقتور پکسل آرٹ تخلیق کا آلہ ہے۔ اپنی اپنی پیلیٹ تحریر کریں۔ شفافیت کے ساتھ اسپرٹ بنائیں ، پکسل کے حساب سے۔ بڑی بڑی تصاویر ، جگہ جگہ بنائیں۔ دوستوں کے ساتھ ان کا اشتراک کریں! مزید خصوصیات آرہی ہیں! دریافت کریں ... پیلیٹوں کو دوبارہ صاف کریں!
ایپ پرانے آلات (Android ورژن 4.1.0 اور اس سے زیادہ) کی تائید کرتی ہے۔

#    'jp',
レーピンのパレット
＃ 簡単な説明
小さなピクセルアートのスプライトを作成するアプリ
＃＃ 完全な説明
これは小さいながらも強力なピクセルアート作成ツールです。 独自のパレットを作成します。 ピクセル単位で透明度のあるスプライトを作成します。 スポットごとに、より大きな画像を作成します。 友達とシェアしよう！ 今後さらに多くの機能があります！ 探索...レーピンのパレットします！
このアプリは古いデバイス（Androidバージョン4.1.0以降）をサポートしています。