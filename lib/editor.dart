import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as ImageLib;
import 'package:flutter/services.dart' show rootBundle;
import 'package:reorderables/reorderables.dart';
import 'app_localizations.dart';
import 'common.dart';
import 'pixel_math.dart';
import 'widgets.dart';
import 'colorwheel.dart';
import 'global.dart' as global;
import 'dart:math' as math;

const PENCIL_TOOL = 1;
const PICKER_TOOL = 2;
const FILL_TOOL = 3;
const PAINT_TOOL = 4;
const PAINT_BEHIND = 5;
const CUT_TOOL = 6;
const PASTE_TOOL = 7;

class EditScreen extends StatefulWidget {
  final ImageLib.Image picture;

  EditScreen({Key key, @required this.picture}) : super(key: key);

  @override
  _EditState createState() => _EditState(picture);
}

class _EditState extends State<EditScreen> {
  _EditState(this._picture);

  ImageLib.Image _picture;
  Future<ui.Image> _imageFuture;

  final TransformationController controller = TransformationController();
  int currentColor = 0xff000000;
  int _cutLineColor = 0xff0000ff;
  final editMode = ValueNotifier(0);
  int editModeLast = 1;

  final GlobalKey _pictureKey = GlobalKey();
  final Map<int, Offset> _touches = Map();
  List<PieceData> _pieces = [];
  List<List<PieceData>> undoRedoBuffer = [];
  int undoRedoIndex = 0;
  Size boxSize = Size.zero;
  List<int> _palette = [];
  double _zoomScale = 1;
  int _oldColor;

  @override
  void initState() {
    super.initState();
    _picture = widget.picture.clone();
    _imageFuture = waitImageFromPicture(_picture);
    _palette = global.prefs.palette;
    print(_palette);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: ValueListenableBuilder(
              valueListenable: editMode,
              builder: (context, value, child) {
                return Stack(children: [
                  Container(
                      decoration: spriteDecoration(MediaQuery.of(context).size),
                      child: Column(children: [
                        Expanded(
                          child: Container(
                            child: InteractiveViewer(
                              transformationController: controller,
                              maxScale: 100,
                              boundaryMargin: EdgeInsets.all(32),
                              panEnabled: this.editMode.value == 0 ? true : false,
                              scaleEnabled: this.editMode.value == 0 ? true : false,
                              onInteractionEnd: (details) {
                                _zoomScale = controller.value.row0[0];
                              },
                              child: Hero(
                                tag: 'box',
                                child: Stack(children: [
                                  new Positioned.fill(
                                    child: Container(
                                      child: LayoutBuilder(
                                        builder: (BuildContext context, BoxConstraints constraints) {
                                          double _constraintsAspectRatio = constraints.maxWidth / constraints.maxHeight;
                                          boxSize = Size(
                                            _picture.width / _picture.height > _constraintsAspectRatio
                                                ? constraints.maxWidth
                                                : constraints.maxHeight * _picture.width / _picture.height,
                                            _picture.width / _picture.height > _constraintsAspectRatio
                                                ? constraints.maxWidth / (_picture.width / _picture.height)
                                                : constraints.maxHeight,
                                          );
                                          return Builder(
                                            key: _pictureKey,
                                            builder: (BuildContext context) {
                                              return Stack(
                                                children: <Widget>[
                                                  Container(
                                                    child: FutureBuilder<ui.Image>(
                                                      future: _imageFuture,
                                                      // future: _loadGridImage(assetPath),
                                                      builder: (context, snapshot) {
                                                        if (snapshot.hasData) {
                                                          return Container(
                                                            child: Container(
                                                              child: CustomPaint(
                                                                painter: UIImagePainter(image: snapshot.data),
                                                                child: Container(),
                                                              ),
                                                            ),
                                                          );
                                                        }
                                                        return Center(
                                                            child: ClipRRect(
                                                                borderRadius: BorderRadius.circular(22),
                                                                child: Container(
                                                                    padding: EdgeInsets.all(44),
                                                                    color: Colors.white,
                                                                    child: Text(global.string(AppString.cannot_show_image)))));
                                                      },
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                  this.editMode.value == 0
                                      ? GestureDetector(
                                          onDoubleTap: () {
                                            var inv = Matrix4.inverted(controller.value);
                                            if (controller.value == inv) {
                                              controller.value = Matrix4.diagonal3Values(3, 3, 1);
                                              // + Matrix4.translationValues(
                                              //         -global.GRID_SIDE.toDouble(), -global.GRID_SIDE.toDouble(), 0);
                                            } else {
                                              controller.value = Matrix4.diagonal3Values(1, 1, 1);
                                            }
                                          },
                                        )
                                      : Listener(
                                          onPointerDown: (details) {
                                            _touches[details.pointer] = details.localPosition;
                                            _onPenDraw(details.localPosition);
                                          },
                                          onPointerMove: (details) {
                                            _touches[details.pointer] = details.localPosition;
                                            _onPenDraw(details.localPosition);
                                          },
                                          onPointerUp: (details) {
                                            _oldColor = null;
                                            _touches.remove(details.pointer);
                                            _onPenDrawEnd();
                                          },
                                          onPointerCancel: (details) {
                                            _oldColor = null;
                                            _touches.remove(details.pointer);
                                            _onPenDrawEnd();
                                          },
                                          child: Container(
                                            color: Colors.transparent,
                                          ),
                                        ),
                                ]),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: AppColor.menuPalette.color,
                          child: Row(
                            children: [
                              Expanded(
                                child: Container(
                                  child: GestureDetector(
                                    onTap: () {
                                      this.editMode.value = 0;
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          height: 32.0 * 2 + 16,
                                          child: DraggableScrollableSheet(
                                              maxChildSize: 1.0,
                                              minChildSize: 1.0,
                                              initialChildSize: 1.0,
                                              builder: (context, scrollController) {
                                                return SingleChildScrollView(
                                                    controller: scrollController,
                                                    scrollDirection: Axis.horizontal,
                                                    child: Container(
                                                      child: Container(
//                                                  width: (_palette.length.toDouble() * 32) / 2 + 32,
                                                        alignment: Alignment.topLeft,
                                                        padding: EdgeInsets.all(8),
                                                        color: AppColor.menuPalette.color,
                                                        child: ReorderableWrap(
                                                          scrollDirection: Axis.horizontal,
                                                          direction: Axis.vertical,
                                                          spacing: 0,
                                                          runSpacing: 0,
                                                          onReorder: (int index, int newIndex) {
                                                            setState(() {
                                                              var c = _palette.removeAt(index);
                                                              _palette.insert(newIndex, c);
                                                            });
                                                          },
                                                          children: _palette.map((x) => _buildColorButton(x)).toList(),
                                                        ),
                                                      ),
                                                    ));
                                              }),
                                        ),
                                        Container(
                                          alignment: Alignment.topLeft,
                                          padding: EdgeInsets.all(8),
                                          color: AppColor.menu.color,
                                          child: Wrap(
                                            spacing: 8,
                                            runSpacing: 4,
                                            children: [
                                              _buildToolButton(context, global.string(AppString.pencil), Icons.edit_rounded, PENCIL_TOOL),
                                              _buildToolButton(
                                                  context, global.string(AppString.picker), Icons.colorize_rounded, PICKER_TOOL),
                                              _buildToolButton(
                                                  context, global.string(AppString.fill), Icons.format_color_fill, FILL_TOOL),
                                              _buildToolButton(context, global.string(AppString.paint), Icons.format_paint, PAINT_TOOL),
                                              _buildToolButton(context, global.string(AppString.paint_behind),
                                                  Icons.format_paint_outlined, PAINT_BEHIND),
                                              // _buildToolButton(context, 'Paint inside tool', Icons.brush, 10),
                                              // _buildToolButton(context, 'Lasso tool', Icons.all_inclusive, 11),
                                              _buildToolButton(context, global.string(AppString.cut), Icons.cut, CUT_TOOL),
                                              _buildToolButton(
                                                  context, global.string(AppString.buffer), Icons.paste, PASTE_TOOL, global.pasteBuffer.isEmpty),
                                              buildFunctionButton(context, global.string(AppString.insert), Icons.insert_drive_file_outlined, () {
                                                if (undoRedoBuffer.length > undoRedoIndex)
                                                  undoRedoBuffer.removeRange(undoRedoIndex, undoRedoBuffer.length);
                                                var buf = global.pasteBuffer.map((p) {
                                                  return PieceData(p.dx + global.pasteX - global.pasteCenterX,
                                                      p.dy + global.pasteY - global.pasteCenterY, p.color);
                                                }).toList();
                                                undoRedoBuffer.add(buf);
                                                ++undoRedoIndex;
                                                setState(() {
                                                  _imageFuture = _waitForChangedImage();
                                                });
                                              }),
                                              buildFunctionButton(context, global.string(AppString.done), Icons.done_all, () {
                                                editMode.value = 0;
                                                setState(() {
                                                  _imageFuture = _waitForChangedImage();
                                                });
                                                global.prefs.palette =
                                                    _palette.length > 0 ? _palette : global.DEFAULT_PALETTE;
                                                Navigator.pop(context, _picture);
                                              }),
                                              if (Platform.isIOS)
                                                buildFunctionButton(context, global.string(AppString.discard), Icons.close, () {
                                                  Navigator.pop(context, widget.picture);
                                                }),
                                              buildFunctionButton(context, global.string(AppString.undo), Icons.undo, () {
                                                if (undoRedoIndex > 0)
                                                  setState(() {
                                                    --undoRedoIndex;
                                                    _imageFuture = _waitForChangedImage();
                                                  });
                                              }, undoRedoBuffer.length == 0 || undoRedoIndex == 0),
                                              buildFunctionButton(context, global.string(AppString.redo), Icons.redo, () {
                                                if (undoRedoIndex < undoRedoBuffer.length)
                                                  setState(() {
                                                    ++undoRedoIndex;
                                                    _imageFuture = _waitForChangedImage();
                                                  });
                                              }, undoRedoIndex == undoRedoBuffer.length),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(6),
                                child: Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Tooltip(
                                        message: global.string(AppString.transparent_color),
                                        child: Container(
                                            width: 32,
                                            height: 32,
                                            decoration: spriteDecoration(Size(32, 32)),
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  currentColor = 0;
                                                });
                                              },
                                            )),
                                      ),
                                      SizedBox(
                                        width: 50,
                                        height: 40,
                                      ),
                                      buildFunctionButton(context, global.string(AppString.delete_color), Icons.delete, () {
                                        setState(() {
                                          _palette.removeWhere((c) => (c == currentColor));
                                          if (_palette.length > 0) currentColor = _palette[0];
                                        });
                                      }),
                                      SizedBox(
                                        width: 44,
                                        height: 8,
                                      ),
                                      currentColor == 0
                                          ? ClipRRect(
                                              borderRadius: BorderRadius.circular(10),
                                              child: Container(
                                                height: 44,
                                                width: 44,
                                                decoration: spriteDecoration(Size(44, 44)),
                                              ),
                                            )
                                          : Tooltip(
                                              message: global.string(AppString.new_color),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10),
                                                child: GestureDetector(
                                                  onTap: () {
                                                    showColorWheel();
                                                  },
                                                  child: Container(
                                                    height: 44,
                                                    width: 44,
                                                    color: colorFromHex(currentColor),
                                                    child: Icon(Icons.add, color: Colors.white),
                                                  ),
                                                ),
                                              ),
                                            ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ])),
                  this.editMode.value != 0
                      ? SizedBox()
                      : Positioned(
                          top: 8,
                          left: 8,
                          child: Container(
                            color: Colors.black,
                            child: Icon(Icons.zoom_out_map_rounded, color: Colors.white, size: 44),
                          ),
                        ),
                ]);
              })),
    );
  }

  void showColorWheel() async {
    final ret = await showDialog<int>(
      barrierDismissible: false,
      context: context,
      builder: (context) => ColorWheelDialog(initialColor: currentColor),
    );
    if (ret != null) {
      setState(() {
        currentColor = ret;
        _palette.removeWhere((c) => (c == ret));
        _palette = [ret] + _palette;
      });
    }
  }

  Widget _buildColorButton(int color) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (editMode.value == 0) editMode.value = editModeLast;
          currentColor = color;
        });
      },
      child: Container(
        width: 32,
        height: 32,
        decoration: BoxDecoration(
          border: color == currentColor
              ? Border.all(color: colorFromHex(currentColor), width: 4)
              : Border.all(color: AppColor.menuPalette.color, width: 4),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          color: colorFromHex(color),
        ),
      ),
    );
  }

  Widget _buildToolButton(BuildContext context, String tooltip, IconData iconData, int buttonNo,
      [bool fadeOut = false]) {
    return Tooltip(
        message: tooltip,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: GestureDetector(
              onTap: () {
                if (fadeOut) return;
                if (editMode.value == buttonNo) {
                  editMode.value = 0;
                } else {
                  editMode.value = buttonNo;
                  editModeLast = buttonNo;
                }
                setState(() {
                  _imageFuture = _waitForChangedImage();
                });
              },
              child: Container(
                child: Icon(iconData, color: Colors.white.withOpacity(fadeOut ? 0.5 : 1)),
                color: editMode.value == buttonNo
                    ? AppColor.buttonActive.color.withOpacity(fadeOut ? 0.5 : 1)
                    : AppColor.button.color.withOpacity(fadeOut ? 0.5 : 1),
                width: 44,
                height: 44,
              )),
        ));
  }

  void _onPenDraw(Offset coords) {
    if (_oldColor == null || _oldColor == currentColor) {
      _oldColor = _picture.getPixel(
          (coords.dx / boxSize.width * _picture.width).toInt(), (coords.dy / boxSize.height * _picture.height).toInt());
      if (_oldColor >> 24 != 0xff) _oldColor = 0x00;
    }
    int radius = 2 + (20 / _zoomScale).toInt();

    var pt = PieceData((coords.dx / boxSize.width * _picture.width).toInt(),
        (coords.dy / boxSize.height * _picture.height).toInt(), this.currentColor);
    switch (editMode.value) {
      case PENCIL_TOOL:
        {
          if (pt.dx <= _picture.width && pt.dy <= _picture.height) {
            if (_pieces.isNotEmpty) {
              var pt1 = _pieces.last;
              bresenham(pt1.dx, pt1.dy, pt.dx, pt.dy, (x, y) {
                var p = PieceData(x, y, this.currentColor);
                _addPieces([p]);
              });
            } else {}
          }
          _addPieces([pt]);
        }
        break;

      case PICKER_TOOL:
        {
          var n = pt.dx + _picture.width * pt.dy;
          if (n < _picture.width * _picture.height)
            setState(() {
              currentColor = _picture.data[n];
              editMode.value = editModeLast;
            });
        }
        break;

      case FILL_TOOL:
        {
          editMode.value = 0;
          var arr = floodFill(_picture, currentColor, pt.dx, pt.dy);
          setState(() {
            _pieces.addAll(arr);
          });
        }
        break;

      case PAINT_TOOL:
        {
          var arr = paintTool(_picture, currentColor, pt.dx, pt.dy, radius);
          setState(() {
            _addPieces(arr);
          });
        }
        break;

      case PAINT_BEHIND:
        {
          var arr = paintBehindTool(_picture, currentColor, pt.dx, pt.dy, radius, _oldColor);
          setState(() {
            _addPieces(arr);
          });
        }
        break;

      case CUT_TOOL:
        {
          if (pt.dx <= _picture.width && pt.dy <= _picture.height) {
            if (_pieces.isNotEmpty) {
              var pt1 = _pieces.last;
              bresenham(pt1.dx, pt1.dy, pt.dx, pt.dy, (x, y) {
                var p = PieceData(x, y, _cutLineColor);
                _addPieces([p]);
              });
            } else {}
          }
          _addPieces([pt]);
        }
        break;
      case PASTE_TOOL:
        {
          setState(() {
            global.pasteX = pt.dx;
            global.pasteY = pt.dy;
            _imageFuture = _waitForChangedImage();
          });
        }
        break;

      // /// paint inside tool
      // case 10:
      //   {
      //     var arr = paintInside1(_picture, currentColor, pt2.dx, pt2.dy, radius, _oldColor);
      //     setState(() {
      //       _addPieces(arr);
      //     });
      //   }
      //   break;
      default:
        {}
        break;
    }
  }

  void _addPieces(List<PieceData> ps) {
    for (PieceData p in ps) {
//      int c = _picture.getPixel(p.dx, p.dy);
      if (p.color == 0x00) {
        var p1 = PieceData(p.dx, p.dy, global.gridBackgroundColor(p.dx, p.dy));
        _pieces.add(p1);
        continue;
      }
      // if (c != this.currentColor) {
      _pieces.add(p);
      // }
    }
  }

  void _onPenDrawEnd() {
    if (_pieces.isEmpty) return;
    if (_touches.keys.length > 0) return;

    if (editMode.value == CUT_TOOL) {
      int side = global.GRID_SIDE;
      var bytes = Uint32List(side * side);
      bytes.fillRange(0, bytes.length, 0x00);
      PieceData pt = _pieces[0];
      if (pt.dx <= side && pt.dy <= side) {
        var pt1 = _pieces.last;
        bresenham(pt1.dx, pt1.dy, pt.dx, pt.dy, (x, y) {
          var p = PieceData(x, y, _cutLineColor);
          _addPieces([p]);
        });
      }
      _addPieces([pt]);
      var pic_out = ImageLib.Image.fromBytes(side, side, bytes);
      for (PieceData piece in _pieces) {
        pic_out.setPixel(piece.dx, piece.dy, _cutLineColor);
      }
      var arr = floodFill(pic_out, _cutLineColor, 0, 0);

      pic_out = ImageLib.Image.fromBytes(side, side, bytes);
      for (PieceData piece in arr) {
        pic_out.setPixel(piece.dx, piece.dy, _picture.getPixel(piece.dx, piece.dy));
      }
      _pieces = [];
      global.pasteBuffer = [];
      int color;
      for (int y = 0; y < side; ++y) {
        for (int x = 0; x < side; ++x) {
          if (_picture.getPixel(x, y) != pic_out.getPixel(x, y)) {
            _pieces.add(PieceData(x, y, global.gridBackgroundColor(x, y)));
            color = _picture.getPixel(x, y);
            if (color >> 24 == 0xff) global.pasteBuffer.add(PieceData(x, y, color));
          }
        }
      }
      global.pasteCenterX = global.pasteBuffer.map((m) => m.dx).reduce((a, b) => a + b) ~/ global.pasteBuffer.length;
      global.pasteCenterY = global.pasteBuffer.map((m) => m.dy).reduce((a, b) => a + b) ~/ global.pasteBuffer.length;
      global.pasteX = global.pasteCenterX + 10;
      global.pasteY = global.pasteCenterY - 10;
      editMode.value = PASTE_TOOL;
    }

    setState(() {
      if (undoRedoBuffer.length > undoRedoIndex) undoRedoBuffer.removeRange(undoRedoIndex, undoRedoBuffer.length);
      undoRedoBuffer.add(_pieces);
      ++undoRedoIndex;
      _pieces = [];
      _imageFuture = _waitForChangedImage();
    });
  }

  Future<ui.Image> _waitForChangedImage() async {
    _picture = widget.picture.clone();
    for (var y = 0; y < undoRedoIndex; y++) {
      var ps = undoRedoBuffer[y];
      for (var i = 0; i < ps.length; i++) {
        var piece = ps[i];
        var o = piece.dx + _picture.width * piece.dy;
        if (o < _picture.data.length) _picture.data[o] = piece.color;
      }
    }
    if (editMode.value == PASTE_TOOL) {
      for (var i = 0; i < global.pasteBuffer.length; i++) {
        var piece = global.pasteBuffer[i];
        _picture.setPixelSafe(piece.dx - global.pasteCenterX + global.pasteX, piece.dy - global.pasteCenterY + global.pasteY, piece.color);
      }
    }

    return waitImageFromPicture(_picture);
  }
}
