library repin_palettes.global;

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:repin_palettes/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'common.dart';

// final String adInterstitial = Platform.isAndroid
//     ? 'ca-app-pub-3940256099942544/1033173712'
//     : 'ca-app-pub-3940256099942544/4411468910';
final String admobAppId =
    Platform.isAndroid ? 'ca-app-pub-3151997046354973~4428841198' : 'ca-app-pub-3151997046354973~8631895806';
final String adInterstitial =
    Platform.isAndroid ? 'ca-app-pub-3151997046354973/4101731659' : 'ca-app-pub-3940256099942544/4411468910';
// TODO: replace iOS test ad id with ca-app-pub-3151997046354973/7498955149, register test iOS device

const GRID_SIDE = 512;
const GRID_DARK = 0x11111111;
const GRID_LITE = 0xbbbbbbbb;

const PRO_VERSION = false;
const DEFAULT_PALETTE = [
  4280097057,
  4294967295,
  4285428968,
  4283781854,
  4283383729,
  4290699256,
  4286607718,
  4282988429,
  4284707271,
  4285961952,
  4288468216,
  4282334051,
  4290748553,
  4294566609,
  4293510055,
  4288456942,
  4290958334,
  4284043595,
  4283583618,
  4284244140,
  4285957334,
  4287082987,
  4287670010,
  4282265965
];

List<PieceData> pasteBuffer = [];
int pasteX = 0;
int pasteY = 0;
int pasteCenterX = 0;
int pasteCenterY = 0;

BuildContext context;
Locale locale;

String string(AppString str) {
  if (locale == null) {
    locale = Localizations.of<AppLocalizations>(context, AppLocalizations).locale;
    if (!AppLocalizationsDelegate().isSupported(locale)) {
      locale = Locale('en', '');
    }
  }
  String out = AppLocalizations.arr[locale.languageCode][str.index];
  return out == null ? '' : out;
}

int gridBackgroundColor(int x, int y) {
  if ((x % 16 < 8 && y % 16 > 7) || (x % 16 > 7 && y % 16 < 8)) {
    return GRID_LITE;
  }
  return GRID_DARK;
}

final prefs = SharedPrefs();

class SharedPrefs {
  static SharedPreferences instance;

  init() async {
    if (instance == null) {
      instance = await SharedPreferences.getInstance();
    }
  }

  List<int> get palette => (instance.getStringList('palette') ?? DEFAULT_PALETTE.map((i) => i.toString()).toList())
      .map((i) => int.parse(i))
      .toList();

  set palette(List<int> value) {
    instance.setStringList('palette', value.map((i) => i.toString()).toList());
  }
}

final localDir = LocalDir();

class LocalDir {
  String instance;

  init() async {
    if (instance == null) {
      final directory = await getApplicationDocumentsDirectory();
      instance = '${directory.path}/png';
      new Directory(instance).create();
    }
  }
}

final deviceName = DeviceName();

class DeviceName {
  String instance;

  init() async {
    if (instance == null) {
      final name = await _deviceNameString();
      instance = name;
    }
  }
}

Future<String> _deviceNameString() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String ret = 'unknown device';
  if (Platform.isAndroid) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    ret = '${androidInfo.manufacturer} ${androidInfo.model}';
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    ret = '${iosInfo.utsname.machine}';
  }
  return ret;
}
