import 'package:image/image.dart' as ImageLib;
import 'common.dart';
import 'package:tuple/tuple.dart';
import 'dart:math' as math;
import 'global.dart' as global;

/// Bresenham's line algorithm is a line drawing algorithm that determines
/// the points of an n-dimensional raster that should be selected in order
/// to form a close approximation to a straight line between two points.
bresenham(x0, y0, x1, y1, [fn]) {
  var arr = [];
  if (fn == null) {
    fn = (x, y) {
      arr.add({"x": x, "y": y});
    };
  }
  var dx = x1 - x0;
  var dy = y1 - y0;
  var adx = dx.abs();
  var ady = dy.abs();
  var eps = 0;
  var sx = dx > 0 ? 1 : -1;
  var sy = dy > 0 ? 1 : -1;
  if (adx > ady) {
    for (var x = x0, y = y0; sx < 0 ? x >= x1 : x <= x1; x += sx) {
      fn(x, y);
      eps += ady;
      if ((eps << 1) >= adx) {
        y += sy;
        eps -= adx;
      }
    }
  } else {
    for (var x = x0, y = y0; sy < 0 ? y >= y1 : y <= y1; y += sy) {
      fn(x, y);
      eps += adx;
      if ((eps << 1) >= ady) {
        x += sx;
        eps -= ady;
      }
    }
  }
  return arr;
}

/// The scanline floodfill algorithm using stack instead of recursion, more robust
List<PieceData> floodFill(ImageLib.Image pic, int newColor, int x, int y) {
  List<PieceData> arr = [];
  var oldColor = pic.getPixel(x, y);
  if (colorsEqual(oldColor, newColor)) return arr;
  var w = pic.width;
  var h = pic.height;
  int x1;
  bool spanAbove;
  bool spanBelow;
  List<Tuple2> stack = [];
  stack.add(Tuple2(x, y));
  while (stack.length > 0) {
    x = stack.last.item1;
    y = stack.last.item2;
    stack.removeLast();
    x1 = x;
    while (x1 >= 0 && colorsEqual(pic.getPixel(x1, y), oldColor)) x1--;
    x1++;
    spanAbove = false;
    spanBelow = false;
    while (x1 < w && colorsEqual(pic.getPixel(x1, y), oldColor)) {
      pic.setPixel(x1, y, newColor);
      if (newColor == 0x00) {
        arr.add(PieceData(x1, y, global.gridBackgroundColor(x1, y)));
      } else
      arr.add(PieceData(x1, y, newColor));
      if (!spanAbove && y > 0 && colorsEqual(pic.getPixel(x1, y - 1), oldColor)) {
        stack.add(Tuple2(x1, y - 1));
        spanAbove = true;
      } else if (spanAbove && y > 0 && !colorsEqual(pic.getPixel(x1, y - 1), oldColor)) {
        spanAbove = false;
      }
      if (!spanBelow && y < h - 1 && colorsEqual(pic.getPixel(x1, y + 1), oldColor)) {
        stack.add(Tuple2(x1, y + 1));
        spanBelow = true;
      } else if (spanBelow && y < h - 1 && !colorsEqual(pic.getPixel(x1, y + 1), oldColor)) {
        spanBelow = false;
      }
      x1++;
    }
  }
  return arr;
}

List<PieceData> paintTool(ImageLib.Image pic, int newColor, int center_x, int center_y, int radius) {
  List<PieceData> arr = [];
  int r2 = radius * radius;
  int area = r2 << 2;
  int rr = radius << 1;
  for (int i = 0; i < area; i++) {
    int tx = (i % rr) - radius;
    int ty = (i ~/ rr) - radius;
    if (tx * tx + ty * ty <= r2) arr.add(PieceData(center_x + tx, center_y + ty, newColor));
  }
  return arr;
}

List<PieceData> paintBehindTool(
    ImageLib.Image pic, int newColor, int center_x, int center_y, int radius, int oldColor) {
  List<PieceData> arr = [];
  int r2 = radius * radius;
  int area = r2 << 2;
  int rr = radius << 1;
  for (int i = 0; i < area; i++) {
    int tx = (i % rr) - radius;
    int ty = (i ~/ rr) - radius;
    if (tx * tx + ty * ty <= r2 && colorsEqual(pic.getPixel(center_x + tx, center_y + ty), oldColor))
      arr.add(PieceData(center_x + tx, center_y + ty, newColor));
  }
  return arr;
}

// List<PieceData> paintInside1(ImageLib.Image pic, int newColor, int x, int y, int radius, int oldColor) {
//   List<PieceData> arr = [];
//
//   List<PieceData> bresenham1(x0, y0, x1, y1) {
//     List<PieceData> out = [];
//     var dx = x1 - x0;
//     var dy = y1 - y0;
//     var adx = dx.abs();
//     var ady = dy.abs();
//     var eps = 0;
//     var sx = dx > 0 ? 1 : -1;
//     var sy = dy > 0 ? 1 : -1;
//     if (adx > ady) {
//       for (var x = x0, y = y0; sx < 0 ? x >= x1 : x <= x1; x += sx) {
//         if (pic.getPixel(x, y) != oldColor && pic.getPixel(x, y) != newColor) return out;
//         // if (pic.getPixel(x, y) != oldColor) return out;
//         out.add(PieceData(x, y, newColor));
//         eps += ady;
//         if ((eps << 1) >= adx) {
//           y += sy;
//           eps -= adx;
//         }
//       }
//     } else {
//       for (var x = x0, y = y0; sy < 0 ? y >= y1 : y <= y1; y += sy) {
//         if (pic.getPixel(x, y) != oldColor && pic.getPixel(x, y) != newColor) return out;
//         // if (pic.getPixel(x, y) != oldColor) return out;
//         out.add(PieceData(x, y, newColor));
//         eps += adx;
//
//         if ((eps << 1) >= ady) {
//           x += sx;
//           eps -= ady;
//         }
//       }
//     }
//     return out;
//   }
//
//   // Function to put pixels
//   // at subsequence points
//   void drawCircle(int xc, int yc, int x, int y) {
//     arr.add(PieceData(xc + x, yc + y, newColor));
//     arr.add(PieceData(xc - x, yc + y, newColor));
//     arr.add(PieceData(xc + x, yc - y, newColor));
//     arr.add(PieceData(xc - x, yc - y, newColor));
//     arr.add(PieceData(xc + y, yc + x, newColor));
//     arr.add(PieceData(xc - y, yc + x, newColor));
//     arr.add(PieceData(xc + y, yc - x, newColor));
//     arr.add(PieceData(xc - y, yc - x, newColor));
//
//     // pic.setPixel(xc + x, yc + y, newColor);
//     // pic.setPixel(xc - x, yc + y, newColor);
//     // pic.setPixel(xc + x, yc - y, newColor);
//     // pic.setPixel(xc - x, yc - y, newColor);
//     // pic.setPixel(xc + y, yc + x, newColor);
//     // pic.setPixel(xc - y, yc + x, newColor);
//     // pic.setPixel(xc + y, yc - x, newColor);
//     // pic.setPixel(xc - y, yc - x, newColor);
//   }
//
//   // Function for circle-generation
//   // using Bresenham's algorithm
//   void circleBres(int xc, int yc, int r) {
//     int x = 0, y = r;
//     int d = 3 - 2 * r;
//     drawCircle(xc, yc, x, y);
//     while (y >= x) {
//       x++;
//       if (d > 0) {
//         y--;
//         d = d + 4 * (x - y) + 10;
//       } else
//         d = d + 4 * x + 6;
//       drawCircle(xc, yc, x, y);
//     }
//   }
//
//   circleBres(x, y, radius + 1);
//
//   List<PieceData> arr2 = [];
//
//   for (PieceData p in arr) {
//     arr2.addAll(bresenham1(x, y, p.dx, p.dy));
//   }
//   return arr2;
// }
