import 'package:flutter/material.dart';
import 'dart:ui' as ui;

enum AppColor {
  menu,
  menuPalette,
  button,
  buttonActive,
}

extension AppColorExt on AppColor {
  Color get color {
    return [
      Colors.yellow[100],
      Colors.yellow[100],
      // Color.fromRGBO(0xff, 0xff, 0xff, 0.4),
      Colors.red[200],
      Colors.red[400],
    ][this.index];
  }
}

class UIImagePainter extends CustomPainter {
  final ui.Image image;

  UIImagePainter({this.image});

  @override
  void paint(Canvas canvas, Size size) {
    var portrait = Rect.fromLTWH(0, 0, size.height * image.width.toDouble() / image.height.toDouble(), size.height);
    var landscape = Rect.fromLTWH(0, 0, size.width, size.width * image.height.toDouble() / image.width.toDouble());

    canvas.drawImageRect(
      image,
      Rect.fromLTWH(0, 0, image.width.toDouble(), image.height.toDouble()),
      image.height * size.width / size.height > image.width ? portrait : landscape,
      Paint(),
    );
  }

  @override
  bool shouldRepaint(UIImagePainter oldDelegate) {
    return image != oldDelegate.image;
  }
}

Widget buildFunctionButton(BuildContext context, String tooltip, IconData iconData, fn, [bool fadeOut = false]) {
  return Tooltip(
    message: tooltip,
    child: ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GestureDetector(
          onTap: () {
            if (fadeOut) return;
            fn();
          },
          child: Container(
            child: Icon(iconData, color: Colors.white.withOpacity(fadeOut ? 0.5 : 1)),
            color: AppColor.button.color.withOpacity(fadeOut ? 0.5 : 1),
            width: 44,
            height: 44,
          )),
    ),
  );
}
