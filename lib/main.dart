import 'dart:async';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:image/image.dart' as ImageLib;
import 'package:image_picker/image_picker.dart';
import 'package:share/share.dart';
import 'common.dart';
import 'editor.dart';
import 'widgets.dart';
import 'global.dart' as global;
import 'app_localizations.dart';

//import 'package:image_picker_flutter/image_picker_flutter.dart';
//import 'package:custom_image_picker/custom_image_picker.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await global.prefs.init();
  await global.localDir.init();
  await global.deviceName.init();

  //TODO: leave a few preloaded pics for each localization
  // ByteData b;
  // File newFile;
  // b = await rootBundle.load('assets/art/barrio_chapellin.png');
  // newFile = File(global.localDir.instance + '/img2.png');
  // newFile.writeAsBytesSync(b.buffer.asUint8List());
  //
  // b = await rootBundle.load('assets/art/temptation_tree.png');
  // newFile = File(global.localDir.instance + '/img3.png');
  // newFile.writeAsBytesSync(b.buffer.asUint8List());

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizationsDelegate.supportedLanguages.map((lang) => Locale(lang, '')),
      theme: ThemeData(
          buttonBarTheme: ButtonBarThemeData(
        alignment: MainAxisAlignment.center,
      )),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child:
              Container(decoration: spriteDecoration(WidgetsBinding.instance.window.physicalSize), child: HomeScreen()),
        ),
      ),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  Future<ui.Image> _imageFuture;
  ImageLib.Image _picture;
  Map<int, Offset> _touches = Map();
  final GlobalKey _imageKey = GlobalKey();
  final GlobalKey _boxKey = GlobalKey();
  Size _boxSize = Size(0, 0);

  double _editAreaOffsetX = 0;
  double _editAreaOffsetY = 0;
  int currentPage = 0;
  PageController controller = PageController(
    keepPage: true,
    viewportFraction: 0.75,
  );
  File currentFile;
  Future<ui.Image> _previewImageFuture;
  Future<ui.Image> _previewImageFutureNext;
  Future<ui.Image> _previewImageFuturePrev;
  bool activityIndicatorVisible = false;

  List<FileSystemEntity> get allFiles {
    if (global.localDir.instance == null) return [];
    return Directory(global.localDir.instance).listSync();
  }

  @override
  void initState() {
    super.initState();
    _editAreaOffsetX = 0;
    _editAreaOffsetY = 0;
    FirebaseAdMob.instance.initialize(appId: 'ca-app-pub-3151997046354973~4428841198');
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    global.context = context;
    return FutureBuilder<ui.Image>(
        future: _imageFuture,
        builder: (context, snapshot) {
          _loadImagePreviews();
          return Column(children: [
            Expanded(
              key: _boxKey,
              child: Stack(children: [
                Positioned.fill(
                  child: Container(
                    child: (_imageFuture != null && snapshot.hasData)
                        ? Listener(
                            onPointerDown: (details) {
                              _touches[details.pointer] = details.localPosition;
                            },
                            onPointerMove: (details) {
                              _touches[details.pointer] = details.localPosition;
                              _moveEditArea(details.delta);
                            },
                            onPointerUp: (details) {
                              _touches.remove(details.pointer);
                            },
                            onPointerCancel: (details) {
                              _touches.remove(details.pointer);
                            },
                            child: Container(
                              child: Stack(
                                children: [
                                  Container(
                                    key: _imageKey,
                                    child: CustomPaint(
                                      painter: UIImagePainter(image: snapshot.data),
                                      child: Container(),
                                    ),
                                  ),
                                  Positioned(
                                    left: _editAreaOffsetX.toDouble(),
                                    top: _editAreaOffsetY.toDouble(),
                                    child: SizedBox(
                                      child: Hero(
                                        tag: 'editFrame',
                                        child: Container(
                                            width: _boxSize.width,
                                            height: _boxSize.height,
                                            color: (_picture.width > global.GRID_SIDE ||
                                                    _picture.height > global.GRID_SIDE)
                                                ? Colors.blueGrey.withOpacity(0.35)
                                                : Colors.transparent),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : allFiles.length == 0
                            ? Center(
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(22),
                                    child: Container(
                                        padding: EdgeInsets.all(44),
                                        color: Colors.white,
                                        child: Text(global.string(AppString.welcome)))))
                            : GestureDetector(
                                // onDoubleTapDown: (details) {
                                //   _editAreaOffsetX = details.localPosition.dx - global.GRID_SIDE / 2;
                                //   _editAreaOffsetY = details.localPosition.dy - global.GRID_SIDE / 2;
                                // },
                                // onLongPressStart: (details) {
                                //   _editAreaOffsetX = details.localPosition.dx - global.GRID_SIDE / 2;
                                //   _editAreaOffsetY = details.localPosition.dy - global.GRID_SIDE / 2;
                                // },
                                onDoubleTap: () {
                                  _onEditModeTap();
                                },
                                onLongPress: () {
                                  _onEditModeTap();
                                },
                                child: PageView(
                                    physics: activityIndicatorVisible == true
                                        ? NeverScrollableScrollPhysics()
                                        : BouncingScrollPhysics(),
                                    controller: controller,
                                    onPageChanged: (index) {
                                      print((controller.page + 0.5).toInt());
                                      print(index);
                                      print(allFiles[index].path);
                                      setState(() {
                                        currentPage = (controller.page + 0.5).toInt();
                                        currentFile = allFiles[index];
                                        _loadImagePreviews();
                                      });
                                    },
                                    children: allFiles.length == 0
                                        ? [
                                            Container(
                                                child: CupertinoActivityIndicator(
                                              radius: 22,
                                            ))
                                          ]
                                        : [
                                            for (var i = 0; i < allFiles.length; i++)
                                              Container(
                                                  key: new PageStorageKey<String>('page-$i'),
                                                  child: Container(
                                                    child: FutureBuilder<ui.Image>(
                                                        future: i == currentPage
                                                            ? _previewImageFuture
                                                            : i == currentPage - 1
                                                                ? _previewImageFuturePrev
                                                                : i == currentPage + 1
                                                                    ? _previewImageFutureNext
                                                                    : null,
                                                        builder: (context, snapshot) {
                                                          return snapshot.hasData
                                                              ? Stack(children: [
                                                                  Container(
                                                                      child: CustomPaint(
                                                                    painter: UIImagePainter(image: snapshot.data),
                                                                    child: Container(),
                                                                  )),
                                                                  Center(
                                                                    child: activityIndicatorVisible
                                                                        ? CupertinoActivityIndicator(radius: 22)
                                                                        : null,
                                                                  )
                                                                ])
                                                              : Container(
                                                                  child: CupertinoActivityIndicator(
                                                                      radius: 22), // Text(allFiles[i].path),
                                                                );
                                                        }),
                                                  )),
                                          ]),
                              ),
                  ),
                ),
              ]),
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(8),
              color: AppColor.menu.color,
              child: AnimatedCrossFade(
                duration: const Duration(milliseconds: 200),
                crossFadeState: _imageFuture == null ? CrossFadeState.showFirst : CrossFadeState.showSecond,
                firstChild: Wrap(
                  spacing: 8,
                  runSpacing: 8,
                  children: [
                    buildFunctionButton(context, global.string(AppString.edit), Icons.edit, () {
                      _onEditModeTap();
                    }, allFiles.length == 0),
                    buildFunctionButton(context, global.string(AppString.new_), Icons.note_add_rounded, () {
                      setState(() {
                        currentFile = null;
                      });
                      _navigateToEditArea(0, 0);
                    }),
//                    buildFunctionButton(context, AppLocalizations.of(context).share, Icons.share, () {
                    buildFunctionButton(context, global.string(AppString.share), Icons.share, () {
                      //TODO: fix: ad not showing on iOS because of concurrency
                      Share.shareFiles([currentFile.path],
                          text: 'Made with ${global.deviceName.instance} in Repin palettes app');
                      if (global.PRO_VERSION == false) {
                        InterstitialAd ad = InterstitialAd(
                          adUnitId: global.adInterstitial,
                          targetingInfo: MobileAdTargetingInfo(
                            nonPersonalizedAds: true,
                          ),
                          listener: (MobileAdEvent event) {
                            print("InterstitialAd event is $event");
                            // if (event == MobileAdEvent.closed || event == MobileAdEvent.failedToLoad) {
                            // }
                          },
                        );
                        ad.load();
                        ad.show(
                          anchorType: AnchorType.top,
                          anchorOffset: 100.0,
                          horizontalCenterOffset: 0.0,
                        );
                      }
                    }, allFiles.length == 0),
                    if (global.PRO_VERSION)
                      buildFunctionButton(context, global.string(AppString.load), Icons.image_search, () {
                        pickImageFromGallery();
                      }),
                    buildFunctionButton(context, global.string(AppString.delete), Icons.delete_rounded, () {
                      if (currentPage < allFiles.length) _showDeleteAction(context);
                    }, allFiles.length == 0),
                    buildFunctionButton(context, global.string(AppString.duplicate_file), Icons.copy, () {
                      File f = currentFile;
                      var filename = global.localDir.instance +
                          '/' +
                          DateTime.now().millisecondsSinceEpoch.toString().padLeft(16, '0') +
                          '.png';
                      File newF = f.copySync(filename);
                      print(newF);
                      setState(() {
                        allFiles.add(newF);
                      });
                      controller.jumpToPage(allFiles.length - 1);
                    }, allFiles.length == 0),
                  ],
                ),
                secondChild: Wrap(
                  spacing: 8,
                  runSpacing: 8,
                  children: [
                    buildFunctionButton(context, global.string(AppString.edit_selected), Icons.edit, () {
                      var box = _imageKey.currentContext.findRenderObject() as RenderBox;
                      double boxScale = _picture.height * box.size.width / box.size.height > _picture.width
                          ? box.size.height / _picture.height
                          : box.size.width / _picture.width;
                      int xc = _editAreaOffsetX ~/ boxScale;
                      int yc = _editAreaOffsetY ~/ boxScale;
                      _navigateToEditArea(xc, yc);
                    }),
                    buildFunctionButton(context, global.string(AppString.rotate), Icons.rotate_90_degrees_ccw, () {
                      _picture.exif.orientation = 8;
                      setState(() {
                        _picture = ImageLib.bakeOrientation(_picture);
                        _imageFuture = waitImageFromPicture(_picture);
                        _moveEditArea(Offset.zero);
                      });
                    }),
                    buildFunctionButton(context, global.string(AppString.flip), Icons.flip, () {
                      _picture.exif.orientation = 2;
                      setState(() {
                        _picture = ImageLib.bakeOrientation(_picture);
                        _imageFuture = waitImageFromPicture(_picture);
                      });
                    }),
                    buildFunctionButton(context, global.string(AppString.save), Icons.save, () {
                      var data = ImageLib.encodePng(_picture, level: 8);
                      var file = currentFile == null
                          ? File(global.localDir.instance +
                              '/' +
                              DateTime.now().millisecondsSinceEpoch.toString().padLeft(16, '0') +
                              '.png')
                          : currentFile;
                      file.writeAsBytesSync(data);
                      print('$file saved');
                      //TODO: go to current file
                      // int page = _pageWithFile(currentFile);
                      // print(page);
                      // controller.jumpToPage(page);
                      setState(() {
                        _picture = null;
                        _imageFuture = null;
                      });
                      print(allFiles);
                    }),
                    buildFunctionButton(context, global.string(AppString.back), Icons.arrow_back_ios_sharp, () {
                      setState(() {
                        _editAreaOffsetX = 0;
                        _editAreaOffsetY = 0;
                        _picture = null;
                        _imageFuture = null;
                      });
                    }),
                  ],
                ),
              ),
            ),
          ]);
        });
  }

  int _pageWithFile(File file) {
    for (int i = 0; i < allFiles.length; ++i) {
      if (allFiles[i] == file) return i;
    }
    return 0;
  }

  void _showDeleteAction(context) {
    showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return CupertinoActionSheet(
            actions: [
              CupertinoActionSheetAction(
                onPressed: () {
                  Navigator.of(context).pop('ok');
                  controller.previousPage(duration: (Duration(milliseconds: 200)), curve: Curves.easeInOut);
                  currentFile.delete();
                },
                child: Text(global.string(AppString.delete)),
              )
            ],
            cancelButton: CupertinoActionSheetAction(
              onPressed: () {
                Navigator.of(context).pop('bad');
              },
              child: Text(global.string(AppString.cancel)),
            ),
          );
        });
  }

  void _onEditModeTap() async {
    currentFile = allFiles[currentPage];
    setState(() {
      activityIndicatorVisible = true;
    });
    _picture = await compute(pictureFromFile, currentFile as File);
    _imageFuture = waitImageFromPicture(_picture);
    currentPage = 0;
    setState(() {
      activityIndicatorVisible = false;
    });
  }

  void _loadImagePreviews() {
    if (allFiles.length == 0) return;
    _previewImageFuture = waitImageFromFile(allFiles[currentPage]);
    _previewImageFutureNext = currentPage + 1 >= allFiles.length ? null : waitImageFromFile(allFiles[currentPage + 1]);
    _previewImageFuturePrev = currentPage < 1 ? null : waitImageFromFile(allFiles[currentPage - 1]);
  }

  void pickImageFromGallery() async {
    PickedFile file = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    setState(() {
      if (file != null) {
        var _imagePath = file.path;
        _imageFuture = loadPictureFromFile(_imagePath);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<ui.Image> loadPictureFromFile(String path) async {
    if (path == '') return null;
    File file = File(path);
    Uint8List list = await file.readAsBytes();
    ImageLib.Image p = ImageLib.bakeOrientation(ImageLib.decodeImage(list));
    setState(() {
      _picture = p;
    });
    return waitImageFromPicture(p);
  }

  Future<ImageLib.Image> _navigateToEditArea(int xc, int yc) async {
    int side = global.GRID_SIDE;
    var bytes = Uint32List(side * side);
    bytes.fillRange(0, bytes.length, 0x00);
    var pic = ImageLib.Image.fromBytes(side, side, bytes);
    if (_picture == null)
      setState(() {
        _picture = pic;
        _imageFuture = waitImageFromPicture(_picture);
      });
    if (_picture.width < side) xc = -(side - _picture.width) ~/ 2;
    if (_picture.height < side) yc = -(side - _picture.height) ~/ 2;
    int color;
    int x1;
    int y1;
    for (int y = 0; y < side; ++y) {
      for (int x = 0; x < side; ++x) {
        x1 = x + xc;
        y1 = y + yc;
        if (x1 >= 0 && y1 >= 0 && x1 < _picture.width && y1 < _picture.height) {
          color = _picture.getPixel(x1, y1);
          if (color >> 24 != 0xff) color = global.gridBackgroundColor(x, y);
        } else
          color = global.gridBackgroundColor(x, y);
        pic.setPixel(x, y, color);
      }
    }
    Future<ImageLib.Image> ret = Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EditScreen(picture: pic)),
    );
    var completer = Completer<ImageLib.Image>();
    completer.complete(ret);
    ImageLib.Image retpic = await completer.future;
    if (retpic == null || ret == null) return null;
    int xc1 = 0;
    int yc1 = 0;
    int w = side;
    if (_picture.width > w) w = _picture.width;
    if (xc < 0) {
      w -= xc;
      xc1 = -xc;
    }
    if (xc + side >= _picture.width) {
      w += _picture.width - xc;
    }
    int h = side;
    if (_picture.height > h) h = _picture.height;
    if (yc < 0) {
      h -= yc;
      yc1 = -yc;
    }
    if (yc + side >= _picture.height) {
      h += _picture.height - yc;
    }
    var bytes1 = Uint32List(w * h);
    bytes1.fillRange(0, bytes1.length, 0x00);
    var pic1 = ImageLib.Image.fromBytes(w, h, bytes1);
    for (int y = 0; y < _picture.height; ++y) {
      for (int x = 0; x < _picture.width; ++x) {
        color = _picture.getPixel(x, y);
        pic1.setPixel(x + xc1, y + yc1, color);
      }
    }
    for (int y = 0; y < retpic.height; ++y) {
      for (int x = 0; x < retpic.width; ++x) {
        color = retpic.getPixel(x, y);
        if (color >> 24 != 0xff) color = 0x00;
        pic1.setPixel(x + xc + xc1, y + yc + yc1, color);
      }
    }
    int top = cutTransparencyTop(pic1);
    int bottom = cutTransparencyBottom(pic1);
    int left = cutTransparencyLeft(pic1);
    int right = cutTransparencyRight(pic1);
    pic = ImageLib.Image(right - left, bottom - top);
    if (top == 0 && bottom == 0 && left == 0 && right == 0) {
      setState(() {
        _picture = null;
        _imageFuture = null;
      });
      return null;
    }
    for (int y = 0; y < pic.height; ++y) {
      for (int x = 0; x < pic.width; ++x) {
        int color = pic1.getPixel(x + left, y + top);
        pic.setPixel(x, y, color);
      }
    }
    setState(() {
      _picture = pic;
      _imageFuture = waitImageFromPicture(_picture);
    });
    return ret;
  }

  void _moveEditArea(Offset delta) {
    var box = _imageKey.currentContext.findRenderObject() as RenderBox;
    if (!box.hasSize || _picture == null) return;
    double boxScale = _picture.height * box.size.width / box.size.height > _picture.width
        ? box.size.height / _picture.height
        : box.size.width / _picture.width;
    var offsetX = _editAreaOffsetX + delta.dx;
    var offsetY = _editAreaOffsetY + delta.dy;
    double maxX = (_picture.width - global.GRID_SIDE / 2) * boxScale;
    if (offsetX > maxX) offsetX = maxX;
    double maxY = (_picture.height - global.GRID_SIDE / 2) * boxScale;
    if (offsetY > maxY) offsetY = maxY;
    double minXY = -global.GRID_SIDE * boxScale / 2;
    if (offsetX < minXY) offsetX = minXY;
    if (offsetY < minXY) offsetY = minXY;

    setState(() {
      _boxSize = Size(global.GRID_SIDE * boxScale, global.GRID_SIDE * boxScale);
    });
    setState(() {
      _editAreaOffsetX = offsetX;
      _editAreaOffsetY = offsetY;
    });
  }
}
