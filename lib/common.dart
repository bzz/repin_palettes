import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:image/image.dart' as ImageLib;
import 'package:flutter/services.dart' show rootBundle;
import 'global.dart' as global;

class PieceData {
  final int dx;
  final int dy;
  final int color;
  const PieceData(this.dx, this.dy, this.color);
}


/*      */
/* SYNC */
/*      */
Color colorFromHex(int hex) {
  var c = Color(hex);
  return Color.fromARGB(c.alpha, c.blue, c.green, c.red);
}

bool colorsEqual(int a, int b) {
  if (a == b) return true;
  if (a >> 24 < 0xff && b >> 24 < 0xff) return true;
  return false;
}

ImageLib.Image pictureFromFile(File file) {
  Uint8List list = file.readAsBytesSync();
  ImageLib.Image p = ImageLib.bakeOrientation(ImageLib.decodeImage(list));
  return p;
}

BoxDecoration spriteDecoration(Size size) {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment(0, 0),
      tileMode: TileMode.repeated,
      colors:
//      [Colors.grey[100], Colors.grey[300], Colors.grey[100]],
      List<Color>.generate(size.width ~/ 7 , (i) => Colors.grey[i%2 * 100 + 200]),
    ),
  );
}

int cutTransparencyTop(ImageLib.Image p) {
  for (int y = 0; y < p.height; ++y) {
    for (int x = 0; x < p.width; ++x) {
      if (p.getPixel(x, y) != 0x00) {
        return y;
      }
    }
  }
  return 0;
}

int cutTransparencyLeft(ImageLib.Image p) {
  for (int x = 0; x < p.width; ++x) {
    for (int y = 0; y < p.height; ++y) {
      if (p.getPixel(x, y) != 0x00) {
        return x;
      }
    }
  }
  return 0;
}

int cutTransparencyBottom(ImageLib.Image p) {
  for (int y = p.height - 1; y > 0; --y) {
    for (int x = 0; x < p.width; ++x) {
      if (p.getPixel(x, y) != 0x00) {
        return y + 1;
      }
    }
  }
  return 0;
}

int cutTransparencyRight(ImageLib.Image p) {
  for (int x = p.width - 1; x > 0; --x) {
    for (int y = 0; y < p.height; ++y) {
      if (p.getPixel(x, y) != 0x00) {
        return x + 1;
      }
    }
  }
  return 0;
}


/*       */
/* ASYNC */
/*       */
Future<ui.Image> waitImageFromFile(File file) async {
  Uint8List bytes = file.readAsBytesSync();
  ui.Codec codec = await ui.instantiateImageCodec(bytes);
  ui.FrameInfo frame = await codec.getNextFrame();
  return frame.image;
}

Future<ui.Image> waitImageFromPicture(ImageLib.Image p) async {
  final completer = Completer<ui.Image>();
  ui.decodeImageFromPixels(
    p.data.buffer.asUint8List(),
    p.width,
    p.height,
    ui.PixelFormat.rgba8888,
    (ui.Image img) {
      completer.complete(img);
    },
  );
  return completer.future;
}


// void loadPalette(String path) async {
//   var ret = await rootBundle.load(path);
//   var p = ImageLib.decodePng(ret.buffer.asUint8List());
//   setState(() {
//     _palette = p.data.toList(growable: true);
//     if (_palette.length > 0) currentColor = _palette[0];
//   });
// }
//


// Color invertColor(Color c) {
//   return Color.fromRGBO(255 - c.red, 255 - c.green, 255 - c.blue, c.opacity);
// }

// Future<List<int>> getPalette(ImageLib.Image picture) async {
//   var fs = Map.fromIterable(picture.data.toSet(), value: (i) => picture.data.where((v) => v == i).length);
//   var mapEntries = (fs.entries.toList()..sort((a, b) => b.value.compareTo(a.value)));
//   List<int> out = List();
//   for (var x in mapEntries.take(64)) {
//     out.add(x.key);
//   }
//   final completer = Completer<List<int>>();
//   completer.complete(out);
//   return completer.future;
// }
//
