import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'app_localizations.dart';
import 'common.dart';
import 'global.dart' as global;

class ColorWheelDialog extends StatefulWidget {
  final int initialColor;
  const ColorWheelDialog({Key key, this.initialColor}) : super(key: key);
  @override
  _ColorWheelDialogState createState() {
    return _ColorWheelDialogState();
  }
}

class _ColorWheelDialogState extends State<ColorWheelDialog> {
  int _color = 0;
  double r = 0;
  double g = 0;
  double b = 0;
  HSVColor _hsvColor;
  double h = 0;
  double s = 0;
  double v = 0;
  @override
  void initState() {
    super.initState();
    _color = widget.initialColor;
    Color c = colorFromHex(_color);
    r = c.red.toDouble();
    g = c.green.toDouble();
    b = c.blue.toDouble();
    _hsvColor = HSVColor.fromColor(c);
    h = _hsvColor.hue;
    s = _hsvColor.saturation;
    v = _hsvColor.value;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        global.string(AppString.new_color),
        style: TextStyle(
          color: Colors.grey[400],
        ),
      ),
      content: Container(
        //   child: RotatedBox(
        // quarterTurns: 3,
        child: Column(
          children: [
            Text('RGB',
                style: TextStyle(
                  color: Colors.grey[400],
                )),
            CupertinoSlider(
              activeColor: Colors.grey[300],
              thumbColor: Color.fromARGB(0xff, r.toInt(), 0, 0),
              value: r,
              min: 0,
              max: 255,
              divisions: 256,
              onChanged: (value) {
                setState(() {
                  r = value;
                  _setRGBColor();
                });
              },
            ),
            CupertinoSlider(
              activeColor: Colors.grey[300],
              thumbColor: Color.fromARGB(0xff, 0, g.toInt(), 0),
              value: g,
              min: 0,
              max: 255,
              divisions: 256,
              onChanged: (value) {
                setState(() {
                  g = value;
                  _setRGBColor();
                });
              },
            ),
            CupertinoSlider(
              activeColor: Colors.grey[300],
              thumbColor: Color.fromARGB(0xff, 0, 0, b.toInt()),
              value: b,
              min: 0,
              max: 255,
              divisions: 256,
              onChanged: (value) {
                setState(() {
                  b = value;
                  _setRGBColor();
                });
              },
            ),
            Expanded(
              child:
              Container(
                // width: 100,
                // height: 100,
              ),
            ),
            Text('HSV',
                style: TextStyle(
                  color: Colors.grey[400],
                )),
            CupertinoSlider(
              activeColor: Colors.grey[300],
              value: h,
              min: 0,
              max: 360,
              divisions: 360,
              onChanged: (value) {
                setState(() {
                  h = value;
                  _setHSVColor();
                });
              },
            ),
            CupertinoSlider(
              activeColor: Colors.grey[300],
              value: s,
              min: 0,
              max: 1,
              divisions: 360,
              onChanged: (value) {
                setState(() {
                  s = value;
                  _setHSVColor();
                });
              },
            ),
            CupertinoSlider(
              activeColor: Colors.grey[300],
              value: v,
              min: 0,
              max: 1,
              divisions: 360,
              onChanged: (value) {
                setState(() {
                  v = value;
                  _setHSVColor();
                });
              },
            ),
          ],
        ),
      ),
      actions: [
        FlatButton(
          onPressed: () {
            Navigator.pop(context, null);
          },
          child: Container(
            height: 44,
            width: 44,
            color: colorFromHex(widget.initialColor),
            child: Icon(
              Icons.close,
              color: Colors.white,
            ),
          ),
        ),
        FlatButton(
          onPressed: () {
            Navigator.pop(context, _color);
          },
          child: Container(
            height: 44,
            width: 44,
            color: colorFromHex(_color),
            child: Icon(
              Icons.check,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  _setRGBColor() {
    _color = (0xff00 + b.toInt() << 16) + (g.toInt() << 8) + r.toInt();
    Color c = colorFromHex(_color);
    _hsvColor = HSVColor.fromColor(c);
    h = _hsvColor.hue;
    s = _hsvColor.saturation;
    v = _hsvColor.value;
  }

  _setHSVColor() {
    _hsvColor = HSVColor.fromAHSV(1.0, h, s, v);
    Color c = _hsvColor.toColor();
    r = c.red.toDouble();
    g = c.green.toDouble();
    b = c.green.toDouble();
    _color = (0xff00 + b.toInt() << 16) + (g.toInt() << 8) + r.toInt();
  }
}
